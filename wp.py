import os
import logging
import re
from urllib.parse import urlparse, unquote
from hashlib import md5
from random import randint
import datetime
import urllib.parse
import glob
import pytz
from lxml import etree


### TODO: attachments - add these before posts, and build map of hashes to IDs.
#                     - if attached to comment, attach it to its parent instead.
#                     - replace inline images with file paths


def sanitize_title(text):
    """Approximate clone of WordPress's sanitize_title_with_dashes
    https://github.com/WordPress/WordPress/blob/be6aa715fedb64fba8a848706e050f489c56df82/wp-includes/formatting.php#L2204
    """
    text = re.sub(r"<[^>]*?>", "", text)
    text = re.sub(r"%([a-fA-F0-9][a-fA-F0-9])", r"---\1---", text)
    text = text.strip("%")
    text = re.sub(r"---([a-fA-F0-9][a-fA-F0-9])---", r"%\1", text)
    text = text.lower()
    text = re.sub(r"&.+?;", "", text)
    text = text.replace(".", "-")
    text = re.sub(r"[^%a-z0-9 _-]", "", text)
    text = re.sub(r"\s+", "-", text)
    text = re.sub(r"-+", "-", text)
    text = text.strip()

    return text

def sanitize_author(author_name):
    return sanitize_title(author_name)

def sanitize_post_content(post_content):
    return ''.join(c for c in post_content if valid_xml_char_ordinal(c))

def valid_xml_char_ordinal(c):
    """Strip invalid control and null characters from string to go into an XML document.

    https://stackoverflow.com/a/8735509/2251982
    """
    codepoint = ord(c)
    # Conditions ordered by presumed frequency.
    return (
        0x20 <= codepoint <= 0xD7FF or
        codepoint in (0x9, 0xA, 0xD) or
        0xE000 <= codepoint <= 0xFFFD or
        0x10000 <= codepoint <= 0x10FFFF
    )

class WordPressXMLWriter:
    # Namespaces.
    NSMAP = {"wp": "http://wordpress.org/export/1.2/",
             "dc": "http://purl.org/dc/elements/1.1/",
             "wfw": "http://wellformedweb.org/CommentAPI/",
             "content": "http://purl.org/rss/1.0/modules/content/",
             "excerpt": "http://wordpress.org/export/1.2/excerpt/"}

    WP_PUB_DATE_FORMAT = r"%a, %d %b %Y %H:%M:%S %z"
    WP_POST_DATE_FORMAT = r"%Y-%m-%d %H:%M:%S"
    WP_POST_DATE_GMT_FORMAT = r"%Y-%m-%d %H:%M:%S"

    COAUTHOR_TAXONOMY = "ssl-alp-coauthor"

    def __init__(self, site_title, site_id, base_network_url, base_url,
                 base_source_media_url, existing_post_ids, debug_log_file=None):
        if not base_url.endswith("/"):
            base_url += "/"

        if not base_network_url.endswith("/"):
            base_network_url += "/"

        if not base_source_media_url.endswith("/"):
            base_source_media_url += "/"

        self.title = site_title
        self.site_id = int(site_id)
        self.base_network_url = base_network_url
        self.base_url = base_url
        self.base_source_media_url = base_source_media_url
        self.existing_post_ids = list(existing_post_ids)

        self.document = None
        self.channel = None

        self.last_comment_id = 0
        self.last_term_id = 0

        # Maps.
        self._attachment_filename_hashes_to_post_ids = {}

        self.authors = {}
        self.categories = {}
        self.posts = {}
        self.comments = {}
        self.tags = {}
        self.attachments = {}
        
        self.ninvalidattachment = 0

        self.nimages = 0
        self.nurls = 0

        self.ncategoryrels = 0
        self.ntagrels = 0

        self._setup_logging(debug_log_file)
        self._create_document()

    @property
    def nauthors(self):
        return len(self.authors)
    
    @property
    def ncategories(self):
        return len(self.categories)
    
    @property
    def nposts(self):
        return len(self.posts)

    @property
    def ncomments(self):
        return len(self.comments)

    @property
    def ntags(self):
        return len(self.tags)

    @property
    def nattachments(self):
        return len(self.attachments)

    def _setup_logging(self, debug_log_file):
        # Base logger.
        self.logger = logging.getLogger("wp")
        self.logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter("%(name)-8s - %(levelname)-8s - %(message)s")

        # Log INFO or higher to stdout.
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(formatter)
        stream_handler.setLevel(logging.INFO)
        self.logger.addHandler(stream_handler)

        if debug_log_file is not None:
            # Delete existing log file.
            if os.path.exists(debug_log_file):
                os.remove(debug_log_file)
                
            # Log DEBUG or higher to file.
            file_handler = logging.FileHandler(debug_log_file)
            file_handler.setFormatter(formatter)
            file_handler.setLevel(logging.DEBUG)
            self.logger.addHandler(file_handler)

    def _create_document(self):
        # Current time.
        now = datetime.datetime.now(pytz.utc)

        document = etree.Element("rss", version="2.0", nsmap=self.NSMAP)
        channel = etree.SubElement(document, "channel")
        title = etree.SubElement(channel, "title")
        title.text = self.title
        link = etree.SubElement(channel, "link")
        link.text = self.base_url
        description = etree.SubElement(channel, "description")
        description.text = "Logbook"
        pubdate = etree.SubElement(channel, "pubDate")
        pubdate.text = now.strftime(self.WP_PUB_DATE_FORMAT)
        language = etree.SubElement(channel, "language")
        language.text = "en-GB"
        wxr_version = etree.SubElement(channel, "{http://wordpress.org/export/1.2/}wxr_version")
        wxr_version.text = "1.2"
        base_site_url = etree.SubElement(channel, "{http://wordpress.org/export/1.2/}base_site_url")
        base_site_url.text = self.base_network_url
        base_blog_url = etree.SubElement(channel, "{http://wordpress.org/export/1.2/}base_blog_url")
        base_blog_url.text = self.base_url
        generator = etree.SubElement(channel, "generator")
        generator.text = "alog-to-wordpress"

        self.document = document
        self.channel = channel

    def unique_post_id(self):
        """Get unique post id"""
        candidate_post_id = max(self.existing_post_ids) + 1

        while candidate_post_id in self.existing_post_ids:
            candidate_post_id += 1
        
        self.existing_post_ids.append(candidate_post_id)

        return candidate_post_id

    def unique_comment_id(self):
        """Get unique comment id"""
        self.last_comment_id += 1
        return self.last_comment_id

    def unique_term_id(self):
        """Get unique term id"""
        self.last_term_id += 1
        return self.last_term_id

    def author_term_name(self, author_nicename):
        return f"{self.COAUTHOR_TAXONOMY}-{author_nicename}"
    
    def _get_attachment_id_from_filename(self, filename):
        return self._attachment_filename_hashes_to_post_ids[self._filename_hash(filename)]

    def _filename_hash(self, filename):
        return md5(filename.encode("utf-8")).hexdigest()

    def sanitize_post_content(self, post_content):
        # Remove null and control characters.
        post_content = sanitize_post_content(post_content)

        added_attachments = {}

        def cb_post(match):
            return f'href="{self.base_url}?p={match.group(1)}"'

        def cb_attachment(match):
            nonlocal added_attachments

            encoded_filename = match.group(1)
            filename = unquote(encoded_filename)

            self.logger.debug(f"replacing attachment link {filename} (hash {self._filename_hash(filename)})")
            try:
                attachment_id = self._get_attachment_id_from_filename(filename)
            except:
                # Return original.
                self.logger.warning(f"attachment link {filename} not found - using original")
                self.ninvalidattachment += 1
                return match.group(0)

            added_attachments[attachment_id] = encoded_filename

            return f'href="{self.base_url}?p={attachment_id}"'

        # Replace URLs to other reports.
        post_content = re.sub(
            r'href="https?://alog.ligo-wa.caltech.edu/aLOG/(?:index.php)\?callRep=(\d+)"',
            cb_post,
            post_content,
            flags=re.MULTILINE)
        
        # Replace URLs to attachments.
        #post_content = re.sub(
        #    # https://alog.ligo-wa.caltech.edu/aLOG/uploads/17_20100624211728_lg33s1tins.png
        #    r'href="https?://alog.ligo-wa.caltech.edu/aLOG/uploads/([^\"]*)"',
        #    cb_attachment,
        #    post_content,
        #    flags=re.MULTILINE)

        # Add attachment thumbnails.
        #post_content += "<br/><h3>Attachments</h3><ul>"
        #for attachment_id, attachment_filename in added_attachments.items():
        #    post_content += f'<li><a href="{self.base_url}?p={attachment_id}">{attachment_filename}</li>'
        #post_content += "</ul>"

        return post_content

    def generate_author(self, author_id, username, display_name, email):
        # URL-friendly username.
        nicename = sanitize_author(username)

        author = {
            "username": nicename, # username is same as nicename
            "nicename": nicename,
            "display_name": display_name,
            "email": email,
            "coauthor_term_id": self.unique_term_id(),
            "coauthor_term_slug": self.author_term_name(nicename),
        }

        self.authors[author_id] = author

        # Author.
        wp_author = etree.SubElement(self.channel, "{http://wordpress.org/export/1.2/}author")
        etree.SubElement(wp_author, "{http://wordpress.org/export/1.2/}author_id").text = etree.CDATA(str(author_id))
        etree.SubElement(wp_author, "{http://wordpress.org/export/1.2/}author_login").text = etree.CDATA(author["nicename"])
        etree.SubElement(wp_author, "{http://wordpress.org/export/1.2/}author_email").text = etree.CDATA(author["email"])
        etree.SubElement(wp_author, "{http://wordpress.org/export/1.2/}author_display_name").text = etree.CDATA(author["display_name"])
        etree.SubElement(wp_author, "{http://wordpress.org/export/1.2/}author_first_name").text = etree.CDATA("")
        etree.SubElement(wp_author, "{http://wordpress.org/export/1.2/}author_last_name").text = etree.CDATA("")

        # Terms have slug: ssl-alp-coauthor-[nicename]
        # and name: [display name]

        # Coauthor term.
        wp_coauthor = etree.SubElement(self.channel, "{http://wordpress.org/export/1.2/}term")
        etree.SubElement(wp_coauthor, "{http://wordpress.org/export/1.2/}term_id").text = etree.CDATA(str(author["coauthor_term_id"]))
        etree.SubElement(wp_coauthor, "{http://wordpress.org/export/1.2/}term_taxonomy").text = etree.CDATA(self.COAUTHOR_TAXONOMY)
        etree.SubElement(wp_coauthor, "{http://wordpress.org/export/1.2/}term_slug").text = etree.CDATA(author["coauthor_term_slug"])
        etree.SubElement(wp_coauthor, "{http://wordpress.org/export/1.2/}term_parent").text = etree.CDATA("")
        etree.SubElement(wp_coauthor, "{http://wordpress.org/export/1.2/}term_name").text = etree.CDATA(author["display_name"])

    def generate_category(self, category_id, name):
        nicename = sanitize_title(name)

        category = {
            "name": name,
            "nicename": nicename,
            "term_id": self.unique_term_id(),
        }

        self.categories[category_id] = category

        wp_category = etree.SubElement(self.channel, "{http://wordpress.org/export/1.2/}category")
        etree.SubElement(wp_category, "{http://wordpress.org/export/1.2/}term_id").text = str(category["term_id"])
        etree.SubElement(wp_category, "{http://wordpress.org/export/1.2/}category_nicename").text = etree.CDATA(category["nicename"])
        etree.SubElement(wp_category, "{http://wordpress.org/export/1.2/}category_parent").text = etree.CDATA("")
        etree.SubElement(wp_category, "{http://wordpress.org/export/1.2/}cat_name").text = etree.CDATA(category["name"])

    def generate_tag(self, tag_id, name):
        nicename = sanitize_title(name)

        tag = {
            "name": name,
            "nicename": nicename,
            "term_id": self.unique_term_id(),
        }

        self.tags[tag_id] = tag

        wp_tag = etree.SubElement(self.channel, "{http://wordpress.org/export/1.2/}tag")
        etree.SubElement(wp_tag, "{http://wordpress.org/export/1.2/}term_id").text = str(tag["term_id"])
        etree.SubElement(wp_tag, "{http://wordpress.org/export/1.2/}tag_slug").text = etree.CDATA(tag["nicename"])
        etree.SubElement(wp_tag, "{http://wordpress.org/export/1.2/}tag_name").text = etree.CDATA(tag["name"])

    def generate_attachment(self, parent_post_id, filename, created, author_id):
        # New post ID.
        attachment_post_id = self.unique_post_id()

        attachment = {
            "filename": filename,
            "created": created,
            "author_id": author_id,
            "parent_id": parent_post_id,
        }

        filename_hash = self._filename_hash(filename)

        self.attachments[attachment_post_id] = attachment
        self._attachment_filename_hashes_to_post_ids[filename_hash] = attachment_post_id

        url = "https://alog.ligo-wa.caltech.edu/aLOG/uploads/" + attachment["filename"]

        author = self.authors[attachment["author_id"]]

        # Create fake WordPress file path, to trick import to use the original modified date.
        # YYYY/MM/filename.jpg
        root, ext = os.path.splitext(filename)
        fake_wp_file_path = attachment["created"].strftime("%Y/%m/") + root + ext.lower()

        # Create item.
        item = etree.SubElement(self.channel, "item")

        self.logger.info(f"adding attachment {attachment['filename']} (created {attachment['created']}, hash {filename_hash})")

        # Title.
        etree.SubElement(item, "title").text = etree.CDATA(attachment["filename"])
        # Publication date.
        etree.SubElement(item, "pubDate").text = attachment["created"].strftime(self.WP_PUB_DATE_FORMAT)
        # Author (username).
        etree.SubElement(item, "{http://purl.org/dc/elements/1.1/}creator").text = etree.CDATA(author["username"])
        # GUID.
        etree.SubElement(item, "guid", isPermaLink="false").text = etree.CDATA(url)
        # Description.
        etree.SubElement(item, "description").text = etree.CDATA("")
        # Content.
        etree.SubElement(item, "{http://purl.org/rss/1.0/modules/content/}encoded").text = etree.CDATA("")
        # Excerpt.
        etree.SubElement(item, "{http://wordpress.org/export/1.2/excerpt/}encoded").text = etree.CDATA("")
        # Post ID.
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}post_id").text = str(attachment_post_id)
        # Post date.
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}post_date").text = etree.CDATA(attachment["created"].strftime(self.WP_POST_DATE_FORMAT))
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}post_date_gmt").text = etree.CDATA(attachment["created"].strftime(self.WP_POST_DATE_GMT_FORMAT))
        # Statuses.
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}comment_status").text = etree.CDATA("closed")
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}ping_status").text = etree.CDATA("closed")
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}post_name").text = etree.CDATA(attachment["filename"])
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}status").text = etree.CDATA("inherit")
        # WP meta.
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}post_parent").text = str(parent_post_id)
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}menu_order").text = "0"
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}post_type").text = etree.CDATA("attachment")
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}post_password").text = ""
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}is_sticky").text = "0"
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}attachment_url").text = etree.CDATA(url)
        media_post_meta = etree.SubElement(item, "{http://wordpress.org/export/1.2/}postmeta")
        etree.SubElement(media_post_meta, "{http://wordpress.org/export/1.2/}meta_key").text = etree.CDATA("_wp_attached_file")
        etree.SubElement(media_post_meta, "{http://wordpress.org/export/1.2/}meta_value").text = etree.CDATA(fake_wp_file_path)

    def generate_post(self, post_id, title, created, author_id, category_id, tags, content, comments, attachments, is_draft):
        post = {
            "title": title,
            "slug": sanitize_title(title),
            "created": created,
            "author_id": author_id,
            "category_id": category_id,
            "tags": tags,
            "is_draft": is_draft,
        }

        self.posts[post_id] = post

        if is_draft:
            status = "draft"
        else:
            status = "publish"

        # Create post XML element.
        item = etree.SubElement(self.channel, "item")

        self.logger.info("adding %s (created %s)", post["title"], post["created"])

        # Generate category.
        category = self.categories[category_id]
        etree.SubElement(item, "category", domain="category", nicename=category["nicename"]).text = etree.CDATA(category["name"])
        self.ncategoryrels += 1
        
        # Generate tags.
        for tag_id in tags:
            tag = self.tags[tag_id]
            etree.SubElement(item, "category", domain="post_tag", nicename=tag["nicename"]).text = etree.CDATA(tag["name"])
            self.ntagrels += 1

        # Generate coauthor.
        author = self.authors[post["author_id"]]
        etree.SubElement(item, "category", domain=self.COAUTHOR_TAXONOMY, nicename=author["coauthor_term_slug"]).text = etree.CDATA(author["username"])

        # media
        #for attachment in post.find("attachments"):
        #    content = self._generate_attachment(attachment, channel, content, post_id, first_author)

        # images
        #for image in post.find("images"):
        #    content = self._generate_image(image, channel, content, post_id, first_author)

        # Clean up content.
        content = self.sanitize_post_content(content)

        # Add attachments.
        if len(attachments):
            attachment_urls = []
            for attachment_filename in attachments:
                attachment_url = f"https://alog.ligo-wa.caltech.edu/aLOG/uploads/{attachment_filename}"
                attachment_urls.append(f'<a href="{attachment_url}">{attachment_filename}</a>')
            attachment_list = "\n".join(f"<li>{attachment}</li>" for attachment in attachment_urls)
            content += f"""
<br/>
<h3>Attachments</h3>
<ul>
{attachment_list}
</ul>"""

        # Title.
        etree.SubElement(item, "title").text = etree.CDATA(post["title"])
        # URL.
        etree.SubElement(item, "link").text = ""
        # Publication date.
        etree.SubElement(item, "pubDate").text = post["created"].strftime(self.WP_PUB_DATE_FORMAT)
        # Author (username).
        etree.SubElement(item, "{http://purl.org/dc/elements/1.1/}creator").text = etree.CDATA(author["username"])
        # GUID.
        etree.SubElement(item, "guid", isPermaLink="false").text = self.base_url + "?p=" + str(post_id)
        # Description.
        etree.SubElement(item, "description").text = etree.CDATA("")
        # Content.
        etree.SubElement(item, "{http://purl.org/rss/1.0/modules/content/}encoded").text = etree.CDATA(content)
        # Excerpt.
        etree.SubElement(item, "{http://wordpress.org/export/1.2/excerpt/}encoded").text = etree.CDATA("")
        # Post ID.
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}post_id").text = str(post_id)
        # Post date.
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}post_date").text = etree.CDATA(post["created"].strftime(self.WP_POST_DATE_FORMAT))
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}post_date_gmt").text = etree.CDATA(post["created"].strftime(self.WP_POST_DATE_GMT_FORMAT))
        # Statuses.
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}comment_status").text = etree.CDATA("open")
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}ping_status").text = etree.CDATA("closed")
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}post_name").text = etree.CDATA(post["slug"])
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}status").text = etree.CDATA(status)
        # WP meta.
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}post_parent").text = "0"
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}menu_order").text = "0"
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}post_type").text = etree.CDATA("post")
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}post_password").text = ""
        etree.SubElement(item, "{http://wordpress.org/export/1.2/}is_sticky").text = "0"

        for comment in comments:
            self._generate_comment(item, post_id, **comment)

    def _generate_comment(self, item, parent_id, comment_id, created, author_id, content):
        comment = {
            "created": created,
            "author_id": author_id,
            "parent_id": parent_id
        }

        self.comments[comment_id] = comment

        self.logger.info("\tadding comment by %s to %s", comment["author_id"], comment["parent_id"])

        author = self.authors[comment["author_id"]]

        # media
        #for attachment in parent.find("attachments"):
        #    content = self._generate_attachment(attachment, channel, content, post_id, response_first_author_nicename)

        # images
        #for image in parent.find("images"):
        #    content = self._generate_image(image, channel, content, post_id, response_first_author_nicename)

        # Clean up content, resolve URLs, etc.
        content = self.sanitize_post_content(content)

        # Create item.
        comment_item = etree.SubElement(item, "{http://wordpress.org/export/1.2/}comment")

        # Comment ID.
        etree.SubElement(comment_item, "{http://wordpress.org/export/1.2/}comment_id").text = str(comment_id)
        # Author.
        etree.SubElement(comment_item, "{http://wordpress.org/export/1.2/}comment_author").text = etree.CDATA(author["display_name"])
        etree.SubElement(comment_item, "{http://wordpress.org/export/1.2/}comment_author_email").text = etree.CDATA(author["email"])
        etree.SubElement(comment_item, "{http://wordpress.org/export/1.2/}comment_author_url").text = ""
        etree.SubElement(comment_item, "{http://wordpress.org/export/1.2/}comment_author_IP").text = ""
        # Date.
        etree.SubElement(comment_item, "{http://wordpress.org/export/1.2/}comment_date").text = etree.CDATA(comment["created"].strftime(self.WP_POST_DATE_FORMAT))
        etree.SubElement(comment_item, "{http://wordpress.org/export/1.2/}comment_date_gmt").text = etree.CDATA(comment["created"].strftime(self.WP_POST_DATE_GMT_FORMAT))
        # Content.
        etree.SubElement(comment_item, "{http://wordpress.org/export/1.2/}comment_content").text = etree.CDATA(content)
        # Other stuff.
        etree.SubElement(comment_item, "{http://wordpress.org/export/1.2/}comment_approved").text = "1"
        etree.SubElement(comment_item, "{http://wordpress.org/export/1.2/}comment_type").text = ""
        etree.SubElement(comment_item, "{http://wordpress.org/export/1.2/}comment_parent").text = "0"
        # User ID.
        etree.SubElement(comment_item, "{http://wordpress.org/export/1.2/}comment_user_id").text = str(author_id)

    def save(self, path):
        with open(path, "wb") as f:
            tree = etree.ElementTree(self.document)
            tree.write(f, pretty_print=True)

    def status(self):
        return f"""
{self.nposts} posts
{self.ncomments} comments
{self.nauthors} authors
{self.ncategories} categories
{self.ncategoryrels} post-category relationships
{self.ntags} tags
{self.ntagrels} post-tag relationships
{self.nattachments} attachments
{self.ninvalidattachment} invalid attachment URLs
"""