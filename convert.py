"""Convert alog to WordPress XML.

Sean Leavey
sean.leavey@ligo.org
"""

import datetime
import sqlite3
from wp import WordPressXMLWriter

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

if __name__ == "__main__":
    connection = sqlite3.connect("alog.db")
    connection.row_factory = dict_factory

    existing_post_ids = []
    for post in connection.execute("SELECT reportID from tblReports"):
        existing_post_ids.append(int(post["reportID"]))

    writer = WordPressXMLWriter(
        "Hanford", 2, "https://alog.attackllama.com/", "https://alog.attackllama.com/h1/",
        "https://path.to.media/", existing_post_ids=existing_post_ids,
        debug_log_file="debug.log"
    )

    striptxt = "@LIGO.ORG"

    # Ignore user ID == 1, because this already exists in the WordPress site.
    for user in connection.execute("SELECT userID, username, email FROM tblUsers WHERE userID <> 1"):
        # Remove "@LIGO.ORG"
        username = user["username"]
        if username.endswith(striptxt):
            username = username[:-len(striptxt)]
        display_name = username

        writer.generate_author(user["userID"], username, display_name, user["email"])
    
    for attachment in connection.execute("""
        SELECT reportFK, filename, date_uploaded, reports.authorFK as authorID, reports.parentFK as parentFK
        FROM tblFiles
        INNER JOIN tblReports as reports
        ON tblFiles.reportFK = reports.reportID
        WHERE reports.authorFK <> 1
    """):
        attachment_created = datetime.datetime.strptime(attachment["date_uploaded"], "%Y-%m-%d %H:%M:%S")

        if attachment["parentFK"] != 0:
            # Use the top level post.
            parent_post_id = attachment["parentFK"]
        else:
            parent_post_id = attachment["reportFK"]

        #writer.generate_attachment(parent_post_id, attachment["filename"], attachment_created, attachment["authorID"])

    for category in connection.execute("SELECT taskID, taskName FROM tblTasks"):
        writer.generate_category(category["taskID"], category["taskName"])

    for tag in connection.execute("SELECT tagID, tag FROM tblTags"):
        writer.generate_tag(tag["tagID"], tag["tag"])

    for post in connection.execute("""
        SELECT reportID, taskFK, reportTitle, reportText, authorFK, postConfirmed, dateAdded
        FROM tblReports
        WHERE parentFK = 0
        AND authorFK <> 1
    """):
        comments = []

        for comment in connection.execute("""
            SELECT reportID, reportText, authorFK, postConfirmed, dateAdded
            FROM tblReports
            WHERE parentFK = ?
            AND authorFK <> 1
        """, (post["reportID"],)):
            is_draft = comment["postConfirmed"] == "0"

            if is_draft:
                # No support for draft comments in WordPress.
                continue
        
            comment_created = datetime.datetime.strptime(comment["dateAdded"], "%Y-%m-%d %H:%M:%S")

            comments.append({
                "comment_id": comment["reportID"],
                "created": comment_created,
                "author_id": comment["authorFK"],
                "content": comment["reportText"],
            })

        tags = []

        for tag in connection.execute("""
            SELECT tagFK
            FROM tblReportTags
            WHERE reportFK = ?
        """, (post["reportID"],)):
            tags.append(tag["tagFK"])

        attachments = []

        for attachment in connection.execute("""
            SELECT filename
            FROM tblFiles
            WHERE reportFK = ?
        """, (post["reportID"],)):
            attachments.append(attachment["filename"])

        created = datetime.datetime.strptime(post["dateAdded"], "%Y-%m-%d %H:%M:%S")
        is_draft = post["postConfirmed"] == "0"
        writer.generate_post(
            post["reportID"], post["reportTitle"], created, post["authorFK"], post["taskFK"], tags, post["reportText"],
            comments, attachments, is_draft
        )

    writer.save("alog.xml")

    print("Generated:")
    print(writer.status())