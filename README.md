# aLOG to WordPress converter
Script to convert an [alog](https://alog.ligo-wa.caltech.edu/aLOG/) database dump to
WordPress XML format which can be imported into a fresh or existing WordPress site
running [ALP](https://alp.attackllama.com/).

**NOTE: the script still has hard-coded URLs in places, meaning it won't work for
URLs other than my test site - this will be fixed at some point.**

## Step 1
Get a MySQL dump of the alog database using the following command on the server:
```bash
mysqldump --skip-extended-insert --compact [options]... db-name > alog.sql
```
Replace `db-name` with the real database name. The flags are important;
the import to sqlite3 will probably have errors without them.

Alternatively, take an existing alog SQL file and import it into a MySQL database
then export it using the above command.

## Step 2
Create an sqlite3 database (on your own computer). I call it `alog.db`. The tool
[sqlitebrowser](https://sqlitebrowser.org/) is useful for this.

## Step 3
Convert the MySQL database dump into sqlite3 formatted SQL. Use e.g.
[mysql2sqlite](https://github.com/dumblob/mysql2sqlite):
```bash
./mysql2sqlite alog.sql | sqlite3 alog.db
```

## Step 4
Set up WordPress in network mode if you have not done so already, and create a new
blog. Note the new blog's site ID (e.g. by hovering over its "Edit" link within the
network admin site page).

Install [WP-CLI](https://wp-cli.org/).

Install and activate WordPress Importer (using the `Tools` page in the admin dashboard)
and [this special plugin](https://gist.github.com/westonruter/69c5813b70855d6f4ed6).

Install and activate [ALP](https://alp.attackllama.com/) as a *network* plugin (i.e.
activate from the network admin plugin page). Once installed, go to the ALP tools page
`on the blog admin area`. There you should run the `Convert user roles` and
`Rebuild coauthors` tools (don't run the rebuild crossreferences tool yet).

## Step 5
Open up the WordPress database (e.g. with phpMyAdmin) and delete all entries in
the `wp_N_posts` and `wp_N_postmeta` tables where `N` is the site ID noted above.

## Step 6
Edit `convert.py` to set the various settings like the new site URL, blog URL, and
site ID, then run it with Python 3:
```bash
python convert.py
```

If there are any errors, these will be printed on screen. Further debug information
is provided in `debug.log`.

If the tool runs successfully, it will produce a file called `alog.xml` in the same
directory. This is the WordPress XML import file.

## Step 7
Copy `alog.xml` to the WordPress server, then run this command there:
```
wp import --path=/path/to/wordpress/base/web/directory --url=https://url/for/blog/ /path/to/alog.xml --authors=create --skip=image_resize --user=your-username --debug
```
Replace `path` with your WordPress web directory path, `url` with the *blog* URL, and `your-username` with your admin username.

The command will take a long time to run - many hours.

## Step 8
Once the command has run, perform some clean-up tasks.

WordPress Importer creates revisions for each imported post for some unknown reason, and
these revisions are made by the first user (i.e. you). Delete these revisions by running
the following SQL query:
```sql
DELETE FROM `wp_N_posts` WHERE `post_type` = 'revision'
```
Replace `N` with the site ID.

Rebuild cross-references. On the server, open up the WP shell:
```bash
wp shell --path=/path/to/wordpress/base/web/directory --url=https://url/for/blog/
```
Inside the prompt, run:
```php
> global $ssl_alp;
> $ssl_alp->references->rebuild_references();
```
This will take a while to run (an hour or a few), and will at the end return `NULL`.

Everything should be ready now on the site.